﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    [SerializeField]
    private Material portalMaterial = null;
    [SerializeField]
    private Camera playerCamera = null;
    [SerializeField]
    private GameObject targetPortalRender = null;
    [SerializeField]
    private GameObject otherPortalRender = null;

    private void Start()
    {
        Camera camera = GetComponent<Camera>();
        camera.targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
        portalMaterial.mainTexture = camera.targetTexture;
    }

    private void Update()
    {
        Vector3 targetPos = new Vector3(targetPortalRender.transform.position.x * targetPortalRender.transform.localScale.x,
                                        targetPortalRender.transform.position.y * targetPortalRender.transform.localScale.z,
                                        targetPortalRender.transform.position.z * targetPortalRender.transform.localScale.y);
        transform.position = targetPos - playerCamera.transform.position + otherPortalRender.transform.position;
        Vector3 rot = playerCamera.transform.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler(rot.x, rot.y + 180.0f, rot.z);
    }
}
