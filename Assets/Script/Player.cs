﻿using UnityEngine;

namespace MazeGame
{
    public class Player : MonoBehaviour
    {
        [SerializeField]
        private float mouseSensitivity = 10.0f;
        [SerializeField]
        private float movementSpeed = 200.0f;
        [SerializeField]
        private float smoothTime = 0.3F;
        [SerializeField]
        private Vector3 cameraOffset = Vector3.zero;

        public float SetXAxis { set { rotMovement.x = value; } }
        public float SetYAxis { set { rotMovement.y = value; } }

        private Vector3 smoothVelocity = Vector3.zero;
        private Vector2 rotMovement = new Vector2(0.0f, 180.0f);
        private Rigidbody rb;

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            rb = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            rotMovement.x -= Input.GetAxisRaw("Mouse Y") * mouseSensitivity;
            rotMovement.x = Mathf.Clamp(rotMovement.x, -90.0f, 90.0f);
            rotMovement.y += Input.GetAxisRaw("Mouse X") * mouseSensitivity;
            if (rotMovement.y >= 360.0f)
            {
                rotMovement.y -= 360.0f;
            }
            else if (rotMovement.y < 0.0f)
            {
                rotMovement.y += 360.0f;
            }

            Vector3 movementDir = Vector3.zero;
            if (Input.GetKey(KeyCode.W))
            {
                movementDir.z = 1.0f;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                movementDir.z = -1.0f;
            }
            if (Input.GetKey(KeyCode.D))
            {
                movementDir.x = 1.0f;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                movementDir.x = -1.0f;
            }

            rb.velocity = Vector3.SmoothDamp(rb.velocity, transform.TransformDirection(movementDir.normalized) * movementSpeed, ref smoothVelocity, smoothTime);
            Quaternion headRotation = Quaternion.Euler(rotMovement.x, rotMovement.y, 0.0f);
            Camera.main.transform.position = transform.position + cameraOffset;
            Camera.main.transform.rotation = headRotation;
            rb.rotation = Quaternion.Euler(0.0f, rotMovement.y, 0.0f);
        }
    }
}